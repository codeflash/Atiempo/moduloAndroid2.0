package codeflash.com.atiempo.Adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

import codeflash.com.atiempo.Models.Ruta;
import codeflash.com.atiempo.Models.RutaResults;
import codeflash.com.atiempo.Models.Rutas;
import codeflash.com.atiempo.R;

/**
 * Created by Luigi on 6/10/2016.
 */
public class RutaAdapter extends BaseAdapter {
    static class ViewHolder{
        TextView latitude;
        TextView longitude;
    }
    private List<Rutas> rutas;
    private LayoutInflater inflater = null;
    public RutaAdapter(Context c, List<Rutas> rutas)
    {
        Log.v("AtiempoAPP", "Constructing CustomAdapter");
        this.rutas = rutas;
        inflater = LayoutInflater.from(c);
    }
    @Override
    public int getCount() {
       return rutas.size();
    }

    @Override
    public Object getItem(int i) {
        return rutas.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        ViewHolder holder;
        if(convertView == null){
            convertView = inflater.inflate(R.layout.list_row_rutas, null);
            holder = new ViewHolder();
            holder.latitude = (TextView) convertView.findViewById(R.id.latitud);
            holder.longitude = (TextView) convertView.findViewById(R.id.longitud);
            convertView.setTag(holder);
        }else{
            holder =  (ViewHolder) convertView.getTag();
        }

        holder.latitude.setText(rutas.get(i).getRuta().getSnappedPoints().get(0).getLocation().getLatitude() + "");
        holder.longitude.setText(rutas.get(i).getRuta().getSnappedPoints().get(0).getLocation().getLongitude() +"");
        return convertView;
    }
}
