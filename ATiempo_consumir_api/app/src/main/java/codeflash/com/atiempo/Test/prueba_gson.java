package codeflash.com.atiempo.Test;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.google.android.gms.maps.model.Polyline;

import java.util.List;

import codeflash.com.atiempo.Adapters.RutaAdapter;
import codeflash.com.atiempo.Models.Ruta;
import codeflash.com.atiempo.Models.RutaResults;
import codeflash.com.atiempo.Models.Rutas;
import codeflash.com.atiempo.R;
import codeflash.com.atiempo.Rest.APIClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class prueba_gson extends AppCompatActivity {
    private List<Rutas> rutas;
    //private List<RutaResults> rutass;
    private ListView Lista;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prueba_gson);
        Lista = (ListView) findViewById(R.id.listViewRutas);
        //getActorsList();

    }



    private void getActorsList() {

        Call<RutaResults> call = APIClient.get().getRutas();

        call.enqueue(new Callback<RutaResults>() {
            @Override
            public void onFailure(Call<RutaResults> call, Throwable t) {
                Log.d("APIPlug", "Error Occured: " + t.getMessage());
            }

            @Override
            public void onResponse(Call<RutaResults> call, Response<RutaResults> response) {
                if(response.isSuccessful()){
                    Log.d("APIPlug", "Successfully response fetched" );

                    rutas=response.body().results;

                    if(rutas.size()>0) {

                        showList();
                    }else{
                        Log.d("APIPlug", "No item found");
                    }
                }

            }
        });
    }

    //Our method to show list
    private void showList() {
        Log.d("APIPlug", "Show List");

        RutaAdapter adapter = new RutaAdapter(prueba_gson.this, rutas);
        Lista.setAdapter(adapter);
    }

}
