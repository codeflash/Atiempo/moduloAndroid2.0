package codeflash.com.atiempo.Rest;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Luigi on 8/10/2016.
 */
public class APIClient {
    private static APIATiempo REST_CLIENT;
    private static final String API_URL = "http://codeflash.cloudapp.net/api/v1/"; //Change according to your API path.

    static {
        setupRestClient();
    }

    private APIClient() {}

    public static APIATiempo get() {
        return REST_CLIENT;
    }

    private static void setupRestClient() {
        //Uncomment these lines below to start logging each request.

        /*
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        httpClient.addInterceptor(logging);
        */

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();


        REST_CLIENT = retrofit.create(APIATiempo.class);
    }

}
