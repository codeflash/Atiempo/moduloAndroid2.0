package codeflash.com.atiempo.Test;

import android.graphics.Color;

import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import codeflash.com.atiempo.Models.Point;
import codeflash.com.atiempo.Models.RutaPost;
import codeflash.com.atiempo.Models.RutaResults;
import codeflash.com.atiempo.Models.Rutas;
import codeflash.com.atiempo.Models.Location;
import codeflash.com.atiempo.R;
import codeflash.com.atiempo.Rest.APIClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {




    private GoogleMap mMap;
    private List<Rutas> rutas;
    private Polyline line;
    private Gson gson;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        //LatLng sydney = new LatLng(-34, 151);
        //mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
        //mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));

        //Log.d("APIPlug", "" + rutas.get(1).getRuta().getPoints().get(1).getLocation().getLatitude());
        getRutaList();
        //Log.d("codeflash" , "" +rutas.size());


    }



    private void getRutaList() {
        //seteamos gson
        gson = new Gson();



        Call<RutaResults> call = APIClient.get().getRutas();

        call.enqueue(new Callback<RutaResults>() {
            @Override
            public void onFailure(Call<RutaResults> call, Throwable t) {
                Log.d("APIPlug", "Error Occured: " + t.getMessage());
            }

            @Override
            public void onResponse(Call<RutaResults> call, Response<RutaResults> response) {
                if(response.isSuccessful()){
                    Log.d("APIPlug", "Successfully response fetched" );

                    rutas=response.body().results;

                    if(rutas.size()>0) {

                        String descripcion = "Ruta prueba con benja";
                        String ruta = gson.toJson(rutas.get(0).getRuta());
                        Call<RutaPost> call1 = APIClient.get().createRuta(descripcion,ruta);
                        call1.enqueue(new Callback<RutaPost>() {
                            @Override
                            public void onResponse(Call<RutaPost> call, Response<RutaPost> response) {
                                Log.d("ruta", response.body().getDescripcion() );
                            }

                            @Override
                            public void onFailure(Call<RutaPost> call, Throwable t) {
                                Log.d("error", t.getMessage() );
                            }
                        });

                        /*
                         * DIbujar Polilinea de la ruta
                         */
                        /*List<Point> points = rutas.get(0).getRuta().getSnappedPoints();
                        LatLng sydney = new LatLng(-16.428029238227,  -71.520927796561);
                        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
                        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney, 14));
                        List<LatLng> locationsPoints = new ArrayList<>();

                        for(Point p : points){
                            Location loc = p.getLocation();
                            LatLng location= new LatLng(loc.getLatitude() , loc.getLongitude());
                            Log.d("APIPlug", location.latitude + " " +location.longitude );
                            locationsPoints.add(location);

                        }
                        line = mMap.addPolyline(new PolylineOptions()
                                .addAll(locationsPoints)
                                .color(Color.RED)
                                .width(10));*/

                    }else{

                    }
                }

            }
        });
    }
}
