package codeflash.com.atiempo.Rest;

import codeflash.com.atiempo.Models.RutaPost;
import codeflash.com.atiempo.Models.RutaResults;
import codeflash.com.atiempo.Models.Rutas;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by Luigi on 8/10/2016.
 */
public interface APIATiempo {

    @GET("ruta")
    Call<RutaResults> getRutas();
    @FormUrlEncoded
    @POST("ruta")
    Call<RutaPost> createRuta(@Field("descripcion") String descripcion , @Field("ruta") String ruta);
}
