package codeflash.com.atiempo.Models;


import java.util.List;

/**
 * Created by Luigi on 8/10/2016.
 */
public class Point {
    private String placeId;
    private Location location;
    private int originalIndex;

    public String getPlaceId() {
        return placeId;
    }

    public void setPlaceId(String placeId) {
        this.placeId = placeId;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public int getOriginalIndex() {
        return originalIndex;
    }

    public void setOriginalIndex(int originalIndex) {
        this.originalIndex = originalIndex;
    }
}