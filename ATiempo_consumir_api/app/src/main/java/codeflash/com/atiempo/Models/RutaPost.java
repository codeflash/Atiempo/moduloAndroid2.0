package codeflash.com.atiempo.Models;

/**
 * Created by Luigi on 11/10/2016.
 */
public class RutaPost {
    private String descripcion;
    private String ruta;

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getRuta() {
        return ruta;
    }

    public void setRuta(String ruta) {
        this.ruta = ruta;
    }
}
