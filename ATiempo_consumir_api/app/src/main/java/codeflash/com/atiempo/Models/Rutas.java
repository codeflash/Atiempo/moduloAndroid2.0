package codeflash.com.atiempo.Models;

import java.util.List;

/**
 * Created by Luigi on 8/10/2016.
 */
public class Rutas {
    private int id;
    private String descripcion;
    private Ruta ruta;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Ruta getRuta() {
        return ruta;
    }

    public void setRuta(Ruta ruta) {
        this.ruta = ruta;
    }

}
