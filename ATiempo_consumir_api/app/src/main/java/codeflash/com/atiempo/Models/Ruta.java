package codeflash.com.atiempo.Models;


import java.util.List;

/**
 * Created by Luigi on 8/10/2016.
 */
public class Ruta {
    private List<Point> snappedPoints;

    public List<Point> getSnappedPoints() {
        return snappedPoints;
    }

    public void setSnappedPoints(List<Point> snappedPoints) {
        this.snappedPoints = snappedPoints;
    }
}
