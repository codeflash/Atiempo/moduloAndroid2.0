
package com.example.david.mapasdegoogle;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.widget.Toast;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;

    PlaceAutocompleteFragment autoFragInicio;
    PlaceAutocompleteFragment autoFragLlegada;
    Marker markerInicio  = null;
    Marker markerLlegada = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        final SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);

        autoFragInicio = (PlaceAutocompleteFragment)
                getFragmentManager().findFragmentById(R.id.fragInicio);
        autoFragLlegada = (PlaceAutocompleteFragment)
                getFragmentManager().findFragmentById(R.id.fragLlegada);

        autoFragInicio.setOnPlaceSelectedListener(new PlaceSelectionListener(){
            @Override
            public void onPlaceSelected (Place place){

                if(markerInicio != null)
                {
                    markerInicio.remove();
                    markerInicio=null;
                }
                if(markerInicio==null)
                {
                    LatLng latlng = place.getLatLng();
                    double lat    = latlng.latitude;
                    double lng    = latlng.longitude;
                    markerInicio = mMap.addMarker(new MarkerOptions()
                            .position(new LatLng(lat, lng))
                            .title(place.getAddress().toString()));

                }

             }

            @Override
            public void onError (Status status){
                Toast.makeText(getApplicationContext(),"Error al buscar el sitio",Toast.LENGTH_LONG).show();
            }
        });
        autoFragLlegada.setOnPlaceSelectedListener(new PlaceSelectionListener(){

            @Override
            public void onPlaceSelected (Place place){

                if(markerLlegada != null)
                {
                    markerLlegada.remove();
                    markerLlegada = null;
                }
                if(markerLlegada == null)
                {
                    LatLng latlng = place.getLatLng();
                    double lat    = latlng.latitude;
                    double lng    = latlng.longitude;
                    markerLlegada = mMap.addMarker(new MarkerOptions()
                            .position(new LatLng(lat, lng))
                            .title(place.getAddress().toString()));
                }

            }

            @Override
            public void onError (Status status){
                Toast.makeText(getApplicationContext(),"Error al buscar el sitio",Toast.LENGTH_LONG).show();
            }
        });

        mapFragment.getMapAsync(this);

    }
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
    }


    }//fin
