package com.example.david.mapasdegoogle;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;

import java.util.Locale;

public class HomeActivity extends AppCompatActivity implements View.OnClickListener {

    private GoogleApiClient mGoogleApiClient;
    private static final int REQUEST_PLACE_PICKER = 1;

    Button ingresar;
    TextView getname;
    TextView getaddress;
    TextView getattributions;
    TextView getid;
    TextView getphonenumber;
    TextView getlatlng;
    TextView getlocale;
    TextView getplacetypes;
    TextView getpricelevel;
    TextView getrating;
    TextView getviewport;
    TextView getwebsiteuri;
    TextView mViewAttributions;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        ingresar = (Button)findViewById(R.id.btningresar);
        ingresar.setOnClickListener(this);

        getname = (TextView)findViewById(R.id.getname);
        getaddress = (TextView)findViewById(R.id.getaddress);
        getattributions = (TextView)findViewById(R.id.getattributions);
        getid = (TextView)findViewById(R.id.getid);
        getphonenumber = (TextView)findViewById(R.id.getphonenumber);
        getlatlng = (TextView)findViewById(R.id.getlatlng);
        getlocale = (TextView)findViewById(R.id.getlocale);
        getplacetypes = (TextView)findViewById(R.id.getplacetypes);
        getpricelevel = (TextView)findViewById(R.id.getprivacelevel);
        getrating = (TextView)findViewById(R.id.getprivacelevel);
        getviewport = (TextView)findViewById(R.id.getviewport);
        getwebsiteuri = (TextView)findViewById(R.id.getwebsiteuri);
        mViewAttributions = (TextView)findViewById(R.id.mViewAttributions);

    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        mGoogleApiClient.disconnect();
        super.onStop();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.btningresar:
                try {
                    PlacePicker.IntentBuilder intentBuilder = new PlacePicker.IntentBuilder();
                    Intent intent = intentBuilder.build(this);
                    startActivityForResult(intent, REQUEST_PLACE_PICKER);
                } catch (GooglePlayServicesRepairableException e) {
                } catch (GooglePlayServicesNotAvailableException e) {
                }
                break;
            default:
                break;
        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == REQUEST_PLACE_PICKER && resultCode == Activity.RESULT_OK) {
            final Place place = PlacePicker.getPlace(data, this);

            final CharSequence name = place.getName();
            final CharSequence address = place.getAddress();
            final CharSequence direscion =place.getAttributions();
            final String id = place.getId();
            final CharSequence phone=place.getPhoneNumber();
            final LatLng latlng = place.getLatLng();
            final Locale locale=place.getLocale();
         //   final List<Integer> placetype=place.getPlaceTypes();
         //   final int  pricelevel=place.getPriceLevel();
         //   final float rating=place.getRating();
          //  final LatLngBounds latlngbound=place.getViewport();
          //  final Uri uri=place.getWebsiteUri();

            String attributions = PlacePicker.getAttributions(data);
            if (attributions == null) {
                attributions = "";
            }

            getname.setText(name);
            getaddress.setText(address);
            getattributions.setText(direscion);
            getid.setText(id);
            getphonenumber.setText(phone);
            Double latitud=latlng.latitude;
            String lati=latitud.toString();
            getlatlng.setText(lati);
            getlocale.setText(locale.getCountry());
         //   getplacetypes.setText(placetype.size());
         //   getpricelevel.setText(pricelevel);
        //    final float rati = rating;
          //  getrating.setText((int) rati);
          //  getviewport.setText(latlngbound.toString());
          //  getwebsiteuri.setText(uri.getEncodedQuery());

            mViewAttributions.setText(Html.fromHtml(attributions));

        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }


}//fin
