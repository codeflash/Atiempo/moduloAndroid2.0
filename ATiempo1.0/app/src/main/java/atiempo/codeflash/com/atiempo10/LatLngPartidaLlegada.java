package atiempo.codeflash.com.atiempo10;

public class LatLngPartidaLlegada {

    private double latInicio;
    private double lngInicio;
    private double latLlegada;
    private double lngLlegada;

    public double getLatInicio() {
        return latInicio;
    }

    public void setLatInicio(double latInicio) {
        this.latInicio = latInicio;
    }

    public double getLngInicio() {
        return lngInicio;
    }

    public void setLngInicio(double lngInicio) {
        this.lngInicio = lngInicio;
    }

    public double getLatLlegada() {
        return latLlegada;
    }

    public void setLatLlegada(double latLlegada) {
        this.latLlegada = latLlegada;
    }

    public double getLngLlegada() {
        return lngLlegada;
    }

    public void setLngLlegada(double lngLlegada) {
        this.lngLlegada = lngLlegada;
    }
}