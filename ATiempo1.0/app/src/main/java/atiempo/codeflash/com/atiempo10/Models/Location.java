package atiempo.codeflash.com.atiempo10.Models;

import java.io.Serializable;

/**
 * Created by Luigi on 8/10/2016.
 */

@SuppressWarnings("serial")
public class Location implements Serializable {
    private double latitude;
    private double longitude;

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
}
