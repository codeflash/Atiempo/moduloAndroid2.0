package atiempo.codeflash.com.atiempo10.Adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import atiempo.codeflash.com.atiempo10.Fragments.ItemFragment.OnListFragmentInteractionListener;
import atiempo.codeflash.com.atiempo10.Models.Rutas;
import atiempo.codeflash.com.atiempo10.R;

public class MyItemRecyclerViewAdapter extends RecyclerView.Adapter<MyItemRecyclerViewAdapter.ViewHolder> {

    private final List<Rutas> mValues;
    private final OnListFragmentInteractionListener mListener;

    public MyItemRecyclerViewAdapter(List<Rutas> items, OnListFragmentInteractionListener listener) {
        mValues = items;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        holder.mItem = mValues.get(position);
        String idRutas = Integer.toString(mValues.get(position).getId());
        holder.mIdView.setText(idRutas);
        holder.mContentView.setText(mValues.get(position).getDescripcion());
        holder.mContentView.setText(mValues.get(position).getDescripcion());
        //adicionado final
        holder.mContentView2.setText(mValues.get(position).getDescripcion());
        holder.imageView.setImageResource(R.drawable.bus);
        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListFragmentInteraction(holder.mItem);

                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mIdView;
        public final TextView mContentView;
        public final TextView mContentView2;
        public final ImageView imageView;
        public Rutas mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            //fragmen xml
            /*
            mIdView = (TextView) view.findViewById(R.id.id);
            mContentView = (TextView) view.findViewById(R.id.content);
            */
            mIdView = (TextView) view.findViewById(R.id.idRutaMostrar);
            mContentView = (TextView) view.findViewById(R.id.contentRutaMostrar);
            mContentView2 = (TextView)view.findViewById(R.id.contentRutaMostrar2);
            imageView = (ImageView) view.findViewById(R.id.imagenBus);

        }

        @Override
        public String toString() {
            return super.toString() + " '" + mContentView.getText() + "'";
        }
    }
}
