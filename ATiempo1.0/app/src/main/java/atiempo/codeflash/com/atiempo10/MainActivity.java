package atiempo.codeflash.com.atiempo10;

import android.content.DialogInterface;
import android.content.Intent;
import android.location.LocationManager;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{


    private Button crearRuta;
    private Button irLugares;
    LocationManager locationManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        locationManager=(LocationManager) getSystemService(LOCATION_SERVICE);
        crearRuta = (Button)findViewById(R.id.btnCrearRuta);
        irLugares = (Button)findViewById(R.id.btnIr);

        crearRuta.setOnClickListener(this);
        irLugares.setOnClickListener(this);

        

    }


    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case  R.id.btnCrearRuta:
                Intent sMapRutas = new Intent(MainActivity.this,MapsRutasActivity.class);
                startActivity(sMapRutas);
                break;
            case R.id.btnIr:
                Intent sMapIr = new Intent(MainActivity.this,MapsActivity.class);
                startActivity(sMapIr);
                break;
            default:
                break;
        }

    }
    
        

}//fin
