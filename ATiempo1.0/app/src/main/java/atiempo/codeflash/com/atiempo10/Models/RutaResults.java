package atiempo.codeflash.com.atiempo10.Models;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Luigi on 8/10/2016.
 */
@SuppressWarnings("serial")
public class RutaResults implements Serializable {
    public List<Rutas> results;
}
