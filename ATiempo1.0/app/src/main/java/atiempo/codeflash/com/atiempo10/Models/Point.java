package atiempo.codeflash.com.atiempo10.Models;


import java.io.Serializable;

/**
 * Created by Luigi on 8/10/2016.
 */
@SuppressWarnings("serial")
public class Point implements Serializable {
    private String placeId;
    private Location location;
    private int originalIndex;

    public String getPlaceId() {
        return placeId;
    }

    public void setPlaceId(String placeId) {
        this.placeId = placeId;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public int getOriginalIndex() {
        return originalIndex;
    }

    public void setOriginalIndex(int originalIndex) {
        this.originalIndex = originalIndex;
    }
}