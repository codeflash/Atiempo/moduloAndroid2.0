package atiempo.codeflash.com.atiempo10;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import atiempo.codeflash.com.atiempo10.Modules.DirectionFinder;
import atiempo.codeflash.com.atiempo10.Modules.DirectionFinderListener;
import atiempo.codeflash.com.atiempo10.Modules.Route;

import atiempo.codeflash.com.atiempo10.Fragments.ItemFragment;
import atiempo.codeflash.com.atiempo10.Models.Point;
import atiempo.codeflash.com.atiempo10.Models.RutaResults;
import atiempo.codeflash.com.atiempo10.Models.Rutas;
import atiempo.codeflash.com.atiempo10.Rest.APIClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback,
        View.OnClickListener,NavigationView.OnNavigationItemSelectedListener
        ,ItemFragment.OnListFragmentInteractionListener,DirectionFinderListener {


    private List<Marker> originMarkers = new ArrayList<>();
    private List<Marker> destinationMarkers = new ArrayList<>();
    private List<Polyline> polylinePaths = new ArrayList<>();
    private ProgressDialog progressDialog;
    public double miLongitud = 0;
    public double miLatitud = 0;



    private GoogleMap mMap;
    private LocationManager locManager;
    private LatLng latLngGps = null;
    private ArrayList<Marker> ALmarkers = new ArrayList<>();
    PlaceAutocompleteFragment autoFragInicio;
    PlaceAutocompleteFragment autoFragLlegada;
    Marker markerInicio = null;
    Marker markerLlegada = null;
    LatLngBounds.Builder builder = new LatLngBounds.Builder();
    LatLngBounds bounds;
    int padding = 300;
    CameraUpdate cu;
    FloatingActionButton miUbicacion;
    Button buscarBuses;
    LatLngPartidaLlegada partidaLlegada = new LatLngPartidaLlegada();
    //Codigo de lista
//PRIMERA FORMA
    //int request_code=1;
    public final static int OPINION_REQUEST_CODE = 1;
    //FIN PRIMERA FORMA
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_maps);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        //inicializando datos de usuario y lugar de llegada
        partidaLlegada.setLatInicio(0);
        partidaLlegada.setLngInicio(0);
        partidaLlegada.setLatLlegada(0);
        partidaLlegada.setLngLlegada(0);

        buscarBuses = (Button)findViewById(R.id.btnBuscarBuses);
        buscarBuses.setOnClickListener(this);
        miUbicacion = (FloatingActionButton) findViewById(R.id.miUbicacion);
        miUbicacion.setOnClickListener(this);
        autoFragInicio = (PlaceAutocompleteFragment)
                getFragmentManager().findFragmentById(R.id.fragInicio);
        autoFragLlegada = (PlaceAutocompleteFragment)
                getFragmentManager().findFragmentById(R.id.fragFinal);
        autoFragInicio.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {

                if (markerInicio != null) {
                    markerInicio.remove();
                    markerInicio = null;
                }
                if (markerInicio == null) {
                    LatLng latlng = place.getLatLng();
                    double lat = latlng.latitude;
                    double lng = latlng.longitude;
                    markerInicio = mMap.addMarker(new MarkerOptions()
                            .position(new LatLng(lat, lng))
                            .title(place.getAddress().toString()));
                    //
                    partidaLlegada.setLatInicio(lat);
                    partidaLlegada.setLngInicio(lng);
                    //
                    ALmarkers.add(markerInicio);

                    if (autoFragLlegada != null) {

                        builder = new LatLngBounds.Builder();
                        for (Marker marker : ALmarkers) {
                            builder.include(marker.getPosition());
                        }
                        bounds = builder.build();
                        cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
                        mMap.moveCamera(cu);

                    } else {
                        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat, lng), 18));
                    }
                }

            }

            @Override
            public void onError(Status status) {
                Toast.makeText(getApplicationContext(), "Error al buscar el sitio", Toast.LENGTH_LONG).show();
            }
        });
        autoFragLlegada.setOnPlaceSelectedListener(new PlaceSelectionListener() {

            @Override
            public void onPlaceSelected(Place place) {

                if (markerLlegada != null) {
                    markerLlegada.remove();
                    markerLlegada = null;
                }
                if (markerLlegada == null) {
                    LatLng latlng = place.getLatLng();
                    double lat = latlng.latitude;
                    double lng = latlng.longitude;
                    markerLlegada = mMap.addMarker(new MarkerOptions()
                            .position(new LatLng(lat, lng))
                            .title(place.getAddress().toString()));
                    //
                    partidaLlegada.setLatLlegada(lat);
                    partidaLlegada.setLngLlegada(lng);
                    //
                    ALmarkers.add(markerLlegada);
                    if (ALmarkers.size() > 1) {
                        builder = new LatLngBounds.Builder();
                        for (Marker marker : ALmarkers) {
                            builder.include(marker.getPosition());
                        }
                        bounds = builder.build();
                        cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
                        mMap.moveCamera(cu);
                    }
                }

            }

            @Override
            public void onError(Status status) {
                Toast.makeText(getApplicationContext(), "Error al buscar el sitio", Toast.LENGTH_LONG).show();
            }
        });

        mapFragment.getMapAsync(this);
        registerLocations();

    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        ///desabilitar los botones por deffecto que aparecen despues de presinar un marker
        mMap.getUiSettings().setMapToolbarEnabled(false);

    }
    //Inico gps
    private void registerLocations() {

        locManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        locManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 100, new MyLocationListener());

    }

    @Override
    public void onDirectionFinderStart() {
       // progressDialog = ProgressDialog.show(this, "Please wait.",
       //         "Finding direction..!", true);

        if (originMarkers != null) {
            for (Marker marker : originMarkers) {
                marker.remove();
            }
        }

        if (destinationMarkers != null) {
            for (Marker marker : destinationMarkers) {
                marker.remove();
            }
        }

        if (polylinePaths != null) {
            for (Polyline polyline:polylinePaths ) {
                polyline.remove();
            }
        }
    }

    @Override
    public void onDirectionFinderSuccess(List<Route>routes) {
      // progressDialog.dismiss();
        polylinePaths = new ArrayList<>();
        originMarkers = new ArrayList<>();
        destinationMarkers = new ArrayList<>();

        for (Route route2 : routes) {
           // mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(route2.startLocation, 16));

            originMarkers.add(mMap.addMarker(new MarkerOptions()
                    .title("camina desde aqui! ")
                    .position(route2.startLocation)));
           /* destinationMarkers.add(mMap.addMarker(new MarkerOptions()
                    .title(route2.endAddress)
                    .position(route2.endLocation)));*/

            PolylineOptions polylineOptions = new PolylineOptions().
                    geodesic(true).
                    color(Color.BLUE).
                    width(10);

            for (int i = 0; i < route2.points.size(); i++)
                polylineOptions.add(route2.points.get(i));

            polylinePaths.add(mMap.addPolyline(polylineOptions));
        }
    }

    public class MyLocationListener implements LocationListener {

        @Override
        public void onLocationChanged(Location location) {

            if (markerInicio == null) {
                double lat = location.getLatitude();
                double lng = location.getLongitude();
                autoFragInicio.setText("Ubicacion Actual");
                markerInicio = mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(lat, lng))
                        .title("Ubicacion Actual"));
                latLngGps = new LatLng(lat, lng);
                //
                partidaLlegada.setLatInicio(lat);
                partidaLlegada.setLngInicio(lng);
                //
                ALmarkers.add(markerInicio);
                if (autoFragLlegada != null) {

                    builder = new LatLngBounds.Builder();
                    for (Marker marker : ALmarkers) {
                        builder.include(marker.getPosition());
                    }
                    bounds = builder.build();
                    cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
                    mMap.moveCamera(cu);

                }else{
                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat, lng), 18));
                }
            }
        }
        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
        }
        @Override
        public void onProviderEnabled(String provider) {
        }
        @Override
        public void onProviderDisabled(String provider) {
        }
    }
    //fin de gps
    //metodos de fragment
    @Override
    public void onListFragmentInteraction(Rutas item) {

    }
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        return false;
    }
    //fin de metodos de fragment
    ///botones de mapa
    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.miUbicacion:

                if (markerInicio != null) {
                    markerInicio.remove();
                    markerInicio = null;
                }
                registerLocations();
                break;
            case R.id.btnBuscarBuses:

                if(partidaLlegada.getLatInicio()!=0 && partidaLlegada.getLngInicio() !=0
                        && partidaLlegada.getLatLlegada() !=0 && partidaLlegada.getLngLlegada()!=0)
                {
                    atiempo.codeflash.com.atiempo10.Models.Location partida = new atiempo.codeflash.com.atiempo10.Models.Location();
                    partida.setLatitude(partidaLlegada.getLatInicio());
                    partida.setLongitude(partidaLlegada.getLngInicio());
                    atiempo.codeflash.com.atiempo10.Models.Location llegada = new atiempo.codeflash.com.atiempo10.Models.Location();
                    llegada.setLatitude(partidaLlegada.getLatLlegada());
                    llegada.setLongitude(partidaLlegada.getLngLlegada());

                    double distance = 0.5;
                    Call<RutaResults> call = APIClient.get().buscarRuta(partida.getLatitude(),partida.getLongitude(),llegada.getLatitude(),llegada.getLongitude(),distance);


                    call.enqueue(new Callback<RutaResults>() {
                        @Override
                        public void onResponse(Call<RutaResults> call, Response<RutaResults> response) {
                            Log.d("ruta", "Mostrando Rutas coincidentes");
                            try{
                                if(response.body().results.size() >= 1) {
                                    List<Rutas> rutas = response.body().results;

                                    Intent intent = new Intent(MapsActivity.this, ListaActivity.class);
                                    // String primer = "primer";
                                    intent.putExtra("listaRutas", (Serializable) rutas);
                                    //Inicio de la actividad esperando un resultado
                                    startActivityForResult(intent,OPINION_REQUEST_CODE);
                                }
                            }catch(Exception e) {
                                Toast.makeText(getApplicationContext(),"No hay rutas coincidentes",Toast.LENGTH_LONG).show();
                            }
                        }
                        @Override
                        public void onFailure(Call<RutaResults> call, Throwable t) {
                            Toast.makeText(getApplicationContext(),"No"+call+""+t.getMessage(),Toast.LENGTH_LONG).show();
                        }
                    });
                }else{
                    Toast.makeText(getApplicationContext(),"Elige el punto de partida y de llegada please",Toast.LENGTH_LONG).show();
                }




                break;
            default:
                break;

        }

    }//fin

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == OPINION_REQUEST_CODE) {
            mMap.clear();
            LatLng inicioP = new LatLng(markerInicio.getPosition().latitude ,markerInicio.getPosition().longitude);
            mMap.addMarker(new MarkerOptions().position(inicioP).title("Ubicación de Inicio "));
            LatLng LlegadaP = new LatLng(markerLlegada.getPosition().latitude ,markerLlegada.getPosition().longitude);
            mMap.addMarker(new MarkerOptions().position(LlegadaP).title("Ubicación de Llegada"));
            if (resultCode == RESULT_OK) {
                List<LatLng> listaPuntos = new ArrayList<>();

                Serializable obj ;

                obj = data.getSerializableExtra("opinion");
                Rutas objRutas =new Rutas();
                objRutas = (Rutas)obj;
                String descripcion = objRutas.getDescripcion();

                List<Point> listapoint=new ArrayList<Point>();
                listapoint= objRutas.getRuta().getSnappedPoints();
                int inicio = objRutas.getPuntosCercanos().getInicio();
                int llegada = objRutas.getPuntosCercanos().getLlegada();


                LatLng Iniciooo =  new LatLng( listapoint.get(inicio).getLocation().getLatitude(),
                        listapoint.get(inicio).getLocation().getLongitude());
                LatLng Finnn =   new LatLng( listapoint.get(llegada).getLocation().getLatitude(),
                        listapoint.get(llegada).getLocation().getLongitude());

                Marker mInicio =  mMap.addMarker(new MarkerOptions()
                        .position(Iniciooo)
                        .title("Inicio de ruta"));
                Marker mLlegada = mMap.addMarker(new MarkerOptions()
                        .position(Finnn)
                        .title("Llegada de ruta"));


                ALmarkers.add(mInicio);
                ALmarkers.add(mLlegada);
                builder = new LatLngBounds.Builder();
                for(Marker marker : ALmarkers){
                    builder.include(marker.getPosition());
                }
                bounds = builder.build();
                cu = CameraUpdateFactory.newLatLngBounds(bounds,padding);
                mMap.moveCamera(cu);
                atiempo.codeflash.com.atiempo10.Models.Location location = new atiempo.codeflash.com.atiempo10.Models.Location();
                Log.d("Inicio" , inicio + "");
                Log.d("Llegada", llegada + "");

              //  new LatLng( listapoint.get(inicio).getLocation().getLatitude(),
                //        listapoint.get(inicio).getLocation().getLongitude());

                String inicioPLat = Double.toString(inicioP.latitude);
                String inicioPLng = Double.toString(inicioP.longitude);
                String LlegadaPLat = Double.toString(LlegadaP.latitude);
                String LlegadaPLng = Double.toString(LlegadaP.longitude);



                String InicioooLat = Double.toString( Iniciooo.latitude);
                String InicioooLng = Double.toString(Iniciooo.longitude);
                String FinnnLat = Double.toString(Finnn.latitude);
                String FinnnLng = Double.toString(Finnn.longitude);

                try {
                    new DirectionFinder(this, inicioPLat+","+inicioPLng,InicioooLat+","+InicioooLng).execute();
                    new DirectionFinder(this, LlegadaPLat+","+LlegadaPLng,FinnnLat+","+FinnnLng).execute();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }

                if(llegada>inicio) {
                    for (int i = inicio; i <= llegada; i++) {
                        location = listapoint.get(i).getLocation();
                        double lat = location.getLatitude();
                        double lng = location.getLongitude();
                        LatLng loca = new LatLng(lat, lng);
                        listaPuntos.add(loca);

                    }
                }else{
                    for (int j = inicio; j >= llegada; j--) {
                        location = listapoint.get(j).getLocation();
                        double lat = location.getLatitude();
                        double lng = location.getLongitude();
                        LatLng loca = new LatLng(lat, lng);
                        listaPuntos.add(loca);


                    }
                }

                /*try {
                    // Log.d("directions", "llegaste hasta aqui. "+inicioP.toString()+"    "+InicioooL);
                    // new DirectionFinder(this, inicioP.toString(),InicioooL).execute();
                    new DirectionFinder(this, inicioPLat+","+inicioPLng,inicioPLat+","+inicioPLng).execute();

                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }*/
                Polyline line = mMap.addPolyline(new PolylineOptions()
                        .addAll(listaPuntos)
                        .color(Color.RED)
                        .width(10));



            }
        }





    }



}
