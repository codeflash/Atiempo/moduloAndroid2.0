package atiempo.codeflash.com.atiempo10.Rest;

import atiempo.codeflash.com.atiempo10.Models.Location;
import atiempo.codeflash.com.atiempo10.Models.RutaPost;
import atiempo.codeflash.com.atiempo10.Models.RutaResults;
import atiempo.codeflash.com.atiempo10.Models.RutaPost;
import atiempo.codeflash.com.atiempo10.Models.RutaResults;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by Luigi on 8/10/2016.
 */
public interface APIATiempo {

    @GET("ruta")
    Call<RutaResults> getRutas();
    @FormUrlEncoded
    @POST("ruta")
    Call<RutaPost> createRuta(@Field("descripcion") String descripcion, @Field("ruta") String ruta);
    @FormUrlEncoded
    @POST("buscar/ruta")
    Call<RutaResults> buscarRuta(@Field("latP") double latP, @Field("lngP") double lngP, @Field("latL") double latL, @Field("lngL") double lngL, @Field("distancia") double distance);
}
