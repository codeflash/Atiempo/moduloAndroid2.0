package atiempo.codeflash.com.atiempo10.Models;


import java.io.Serializable;
import java.util.List;

/**
 * Created by Luigi on 8/10/2016.
 */
@SuppressWarnings("serial")
public class Ruta implements Serializable {
    private List<Point> snappedPoints;

    public List<Point> getSnappedPoints() {
        return snappedPoints;
    }

    public void setSnappedPoints(List<Point> snappedPoints) {
        this.snappedPoints = snappedPoints;
    }
}
