package atiempo.codeflash.com.atiempo10;

import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import atiempo.codeflash.com.atiempo10.Models.Point;
import atiempo.codeflash.com.atiempo10.Models.RutaPost;
import atiempo.codeflash.com.atiempo10.Models.RutaResults;
import atiempo.codeflash.com.atiempo10.Models.Rutas;
import atiempo.codeflash.com.atiempo10.Rest.APIClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MapsRutasActivity extends FragmentActivity implements OnMapReadyCallback,View.OnClickListener {

    private GoogleMap mMap;
    private String cadena;
    //private EditText consulta;
    //private ObtenerWebService hiloconexion;
    private aaa hiloconexion;
    private LocationManager locManager;
   // int c=0;
    int i=0;
    boolean pause = false;
    boolean finalizado =false;
    int posiciones=20;
    int e=0;
    List<LatLng> positions = new ArrayList<>(); ;
    TextView lattxt;
    TextView lngtxt;
    Button finalizar;
    Button detener;
    Button reanudar;
    private List<Rutas> rutas;
    private Polyline line;
    private Gson gson;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps_rutas);

        lattxt = (TextView) findViewById(R.id.lat);
        lngtxt= (TextView) findViewById(R.id.lng);
        detener=(Button) findViewById(R.id.btnForce);
        reanudar=(Button) findViewById(R.id.btnResume);
        finalizar=(Button) findViewById(R.id.btnFinal);
        detener.setOnClickListener(this);
        reanudar.setOnClickListener(this);
        finalizar.setOnClickListener(this);
        registerLocations();

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        LatLng arequipa = new LatLng(-16.428386, -71.521210);
        // Add a marker in Sydney and move the camera
        mMap.addMarker(new MarkerOptions()
                .position(arequipa)
                .title("Jose luis bustamente"));

        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(arequipa, 18));
    }
    private void registerLocations() {

        locManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        locManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 2000, 0,new MyLocationListener());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnForce:

                pause = true;
                //finalize();
                break;
            case R.id.btnResume:
                pause=false;
                break;
            case R.id.btnFinal:
                hiloconexion = new aaa();
                hiloconexion.execute(positions);
                finalizado=true;
                break;
            default:
                break;
        }
    }

    public class MyLocationListener  implements LocationListener {


        @Override
        public void onLocationChanged(Location location) {

            if(pause==false && finalizado==false){

            i=positions.size();
            Double valorLatitud=location.getLatitude();
            Double valorLongitud=location.getLongitude();
            positions.add(new LatLng(valorLatitud, valorLongitud));
            //a[i]=new LatLng(valorLatitud,valorLongitud);
            Log.d("codeflash" , positions.size() + "" );
            if(positions.size() > i) {

                mMap.addMarker(new MarkerOptions()
                        .position(positions.get(i))
                        .title("Punto"));
                    mMap.addPolyline(new PolylineOptions().add(positions.get(e), positions.get(i))
                            .width(10)
                            .color(Color.RED)
                    );


            }
            e=i;
            Log.d("nro de posiciones" , i + "" );
            //i++;

            String lat="";
            String lng="";
            lat=String.valueOf(valorLatitud);
            lng=String.valueOf(valorLongitud);
            lattxt.setText(lat);
            lngtxt.setText(lng);

        }
            if (i == posiciones) {
                //hiloconexion = new ObtenerWebService();
                hiloconexion = new aaa();
                hiloconexion.execute(positions);
                finalizado=true;
                //i=0;
                //e=0;
            }

        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onProviderDisabled(String provider) {

        }
    }

    public class aaa extends AsyncTask<List<LatLng> , Integer , String> {

        @Override
        protected String doInBackground(List<LatLng>... params) {
            String key = "AIzaSyDdm9-xc0Y3kh6oKouN6d9_qDWT7RThGgo";
            String devuelve = "";
            i=0;
            cadena = "https://roads.googleapis.com/v1/snapToRoads?path=";
            for(LatLng lnLA : params[0]){
                if(params[0].size()-1 == i){
                    cadena = cadena.concat(lnLA.latitude + "");
                    cadena = cadena.concat(",");
                    cadena = cadena.concat(lnLA.longitude + "");
                }else{
                    cadena = cadena.concat(lnLA.latitude + "");
                    cadena = cadena.concat(",");
                    cadena = cadena.concat(lnLA.longitude + "");
                    cadena = cadena.concat("|");
                }
                i++;
            }
            cadena = cadena.concat("&interpolate=true");
            cadena = cadena.concat("&key="+key);
            Log.d("codeflash1" , cadena);
            URL url = null; // Url de donde queremos obtener información
            try {
                url = new URL(cadena);
                cadena="";
                HttpURLConnection connection = (HttpURLConnection) url.openConnection(); //Abrir la conexión
                connection.setRequestProperty("User-Agent", "Mozilla/5.0" +
                        " (Linux; Android 1.5; es-ES) Ejemplo HTTP");
                //connection.setHeader("content-type", "application/json");

                int respuesta = connection.getResponseCode();
                StringBuilder result = new StringBuilder();
                if (respuesta == HttpURLConnection.HTTP_OK) {
                    Log.d("respuestaHTTP:", "Se envio al roads con exito ");
                }

                   InputStream in = new BufferedInputStream(connection.getInputStream());  // preparo la cadena de entrada
                    BufferedReader reader = new BufferedReader(new InputStreamReader(in));  // la introduzco en un BufferedReader
                    String line;
                    while ((line = reader.readLine()) != null) {
                        result.append(line);        // Paso toda la entrada al StringBuilder
                    }


                   final String respuestaRoads =result.toString();



                    getRutaList(respuestaRoads);
                //APIClient.get().createRuta("Ruta prueba con Yazid HALAJAM AKBHAR!!!",respuestaRoads);


/*
                            List<LatLng> list = PolyUtil.decode(result.toString());
                    PolylineOptions options=new PolylineOptions().width(10).color(Color.BLACK);
                    options.addAll(list);
                    mMap.addPolyline(options);

                    JSONArray jsonSnapeds = respuestaJSON.getJSONArray("snappedPoints");
                    LatLng obj1 = new LatLng(-16.427512117883726,-71.5219058722548);
                    // Route objroute = new Route(obj1);

                    for(int i=0;i<= jsonSnapeds.length()-1 ;i++){
                        JSONObject jsonSnaped = jsonSnapeds.getJSONObject(i);
                        JSONObject jsonLocation = jsonSnaped.getJSONObject("location");
                        Log.d("codeflash",jsonLocation.getString("latitude") +" y "+jsonLocation.getString("longitude"));

                    }


                    //public void correccionR(List <LatLng> ruta){

                    //      PolylineOptions options=new PolylineOptions().width(10).color(Color.BLACK);
                    //     options.addAll(ruta);
                    //   mMap.addPolyline(options);
                    //}


                    //  correccionR(ruta);
                }

*/

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return devuelve;



        }
    }


    private void getRutaList(final String respuestaRoads) {
        //seteamos gson
        gson = new Gson();



        Call<RutaResults> call = APIClient.get().getRutas();



        call.enqueue(new Callback<RutaResults>() {
            @Override
            public void onFailure(Call<RutaResults> call, Throwable t) {
                Log.d("APIPlug", "Error Occured: " + t.getMessage());
            }

            @Override
            public void onResponse(Call<RutaResults> call, Response<RutaResults> response) {
                if(response.isSuccessful()){
                    Log.d("APIPlug", "Successfully response fetched" );

                    rutas=response.body().results;

                    if(rutas.size()>0) {

                        String descripcion = "Ruta prueba con Yazid HALAJAM AKBHAR!!!";
                        String ruta = respuestaRoads; //gson.toJson(rutas.get(0).getRuta())
                        Call<RutaPost> call1 = APIClient.get().createRuta(descripcion,ruta);
                        call1.enqueue(new Callback<RutaPost>() {
                            @Override
                            public void onResponse(Call<RutaPost> call, Response<RutaPost> response) {
                                Log.d("ruta", response.body().getDescripcion() );
                            }

                            @Override
                            public void onFailure(Call<RutaPost> call, Throwable t) {
                                Log.d("error", t.getMessage() );
                            }
                        });

                        /*
                         * DIbujar Polilinea de la ruta
                         */
                        List<Point> points = rutas.get(2).getRuta().getSnappedPoints();
                      //  LatLng sydney = new LatLng(-16.428029238227,  -71.520927796561);
                       // mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
                      //  mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney, 14));
                        List<LatLng> locationsPoints = new ArrayList<>();

                        for(Point p : points){

                            atiempo.codeflash.com.atiempo10.Models.Location loc = p.getLocation();
                            LatLng location= new LatLng(loc.getLatitude() , loc.getLongitude());
                            Log.d("APIPlug", location.latitude + " " +location.longitude );
                            locationsPoints.add(location);

                        }
                        line = mMap.addPolyline(new PolylineOptions()
                                .addAll(locationsPoints)
                                .color(Color.GREEN)
                                .width(10));

                    }else{

                    }
                }

            }
        });
    }





}






