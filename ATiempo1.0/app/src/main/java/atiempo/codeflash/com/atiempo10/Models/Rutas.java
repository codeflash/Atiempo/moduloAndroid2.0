package atiempo.codeflash.com.atiempo10.Models;

import java.io.Serializable;

/**
 * Created by Luigi on 8/10/2016.
 */
@SuppressWarnings("serial")
public class Rutas implements Serializable {
    private int id;
    private String descripcion;
    private Ruta ruta;
    private cp PuntosCercanos;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Ruta getRuta() {
        return ruta;
    }

    public void setRuta(Ruta ruta) {
        this.ruta = ruta;
    }

    public cp getPuntosCercanos() {
        return PuntosCercanos;
    }

    public void setPuntosCercanos(cp puntosCercanos) {
        PuntosCercanos = puntosCercanos;
    }
}
