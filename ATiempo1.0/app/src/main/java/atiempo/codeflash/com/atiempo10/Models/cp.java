package atiempo.codeflash.com.atiempo10.Models;

import java.io.Serializable;

/**
 * Created by Luigi on 15/10/2016.
 */
@SuppressWarnings("serial")
public class cp implements Serializable {
    private int Inicio;
    private int Llegada;

    public int getInicio() {
        return Inicio;
    }

    public void setInicio(int inicio) {
        Inicio = inicio;
    }

    public int getLlegada() {
        return Llegada;
    }

    public void setLlegada(int llegada) {
        Llegada = llegada;
    }


}
