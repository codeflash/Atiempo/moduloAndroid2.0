package atiempo.codeflash.com.atiempo10;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.Toast;

import java.util.List;

import atiempo.codeflash.com.atiempo10.Fragments.ItemFragment;
import atiempo.codeflash.com.atiempo10.Models.Rutas;

public class ListaActivity extends AppCompatActivity implements
        NavigationView.OnNavigationItemSelectedListener
        ,ItemFragment.OnListFragmentInteractionListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista);
        /*
        atiempo.codeflash.com.atiempo10.Models.Location inicio= new atiempo.codeflash.com.atiempo10.Models.Location();
        inicio.setLatitude(-16.447985);//-16.447985, -71.521032
        inicio.setLongitude(-71.521032);
        atiempo.codeflash.com.atiempo10.Models.Location fin = new atiempo.codeflash.com.atiempo10.Models.Location();
        fin.setLatitude(-16.447287);//-16.447287, -71.529156
        fin.setLongitude(-71.529156);

        Point pointInicio = new Point();
        pointInicio.setPlaceId("UNO");
        pointInicio.setOriginalIndex(1);
        pointInicio.setLocation(inicio);

        Point pointFin = new Point();
        pointFin.setPlaceId("DOS");
        pointFin.setOriginalIndex(2);
        pointFin.setLocation(fin);

        List<Point> Lpoint= new ArrayList<Point>();
        Lpoint.add(pointInicio);
        Lpoint.add(pointFin);

        Ruta ruta =new Ruta();
        ruta.setSnappedPoints(Lpoint);

        Rutas rutas = new Rutas();
        rutas.setDescripcion("Transporte A tiempo");
        rutas.setId(1);
        rutas.setRuta(ruta);

        Rutas rutas2 = new Rutas();
        rutas2.setDescripcion("Transporte Davis");
        rutas2.setId(2);
        rutas2.setRuta(ruta);

        List<Rutas> Lrutas=new ArrayList<Rutas>();
        Lrutas.add(rutas);
        Lrutas.add(rutas2);
        */
        List<Rutas> Lrutas= (List<Rutas>) getIntent().getSerializableExtra("listaRutas");
        //Noticia objeto = (Noticia)getIntent().getExtras().getSerializable("parametro");
        if(Lrutas.size() >= 1)
        {
            Fragment fragment = new ItemFragment(Lrutas);
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.content_main,fragment)
                    .commit();
        }else
        {
            Toast.makeText(getApplicationContext(),"No tiene datos",Toast.LENGTH_LONG).show();
        }

    }

    @Override
    public void onListFragmentInteraction(Rutas item) {

        Toast.makeText(getApplicationContext(),"Enviando"+item.getDescripcion(),Toast.LENGTH_LONG).show();
        /*
        String cad = "regresando";
        Intent intentnn = new Intent();
        intentnn.setData(Uri.parse(cad));
        intentnn.putExtra("ListaRutas", item);
        setResult(RESULT_OK, intentnn);

        String cad = "Hola";
        Intent data = new Intent();
        data.setData(Uri.parse(cad));
      //data.putExtra("extra",item);
        setResult(RESULT_OK, data);
    //    finish();
///*
        */
        /*
        Intent intent = new Intent(ListaActivity.this, MapsActivity.class);
        intent.putExtra("david", item);
        startActivity(intent);
        finish();
        */
        /*
        String cad =  Integer.toString(item.getId());
        Intent data = new Intent();
        data.setData(Uri.parse(cad));
        //data.putExtra("extra",item);
        setResult(RESULT_OK, data);
        //    finish();
        */
        //Crear un nuevo intent de respuesta
        Intent databack = new Intent();
        //Añadir como Extra el texto del radiobutton
        databack.putExtra("opinion",item);
        //Devolver por el canal de forma exitosa el mensaje del intent
        setResult(RESULT_OK,databack);
        //Terminar la actividad
        finish();
        /*
        String id="respuesaUno";
        //Crear un nuevo intent de respuesta
        Intent databack = new Intent();
        //Añadir como Extra el texto del radiobutton
        databack.putExtra("opinion",id);
        //Devolver por el canal de forma exitosa el mensaje del intent
        setResult(RESULT_OK,databack);
        //Terminar la actividad
        finish();
        */


    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        return false;
    }
}
